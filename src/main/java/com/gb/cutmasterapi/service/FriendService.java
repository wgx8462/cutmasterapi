package com.gb.cutmasterapi.service;

import com.gb.cutmasterapi.entity.Friend;
import com.gb.cutmasterapi.model.friend.*;
import com.gb.cutmasterapi.repository.FriendRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FriendService {
    private final FriendRepository friendRepository;

    public Friend getData(long id) {
        return friendRepository.findById(id).orElseThrow();
    }

    public void setFriend(FriendCreatRequest request) {
        Friend addData = new Friend();
        addData.setName(request.getName());
        addData.setBirthDay(request.getBirthDay());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setEtcMemo(request.getEtcMemo());
        addData.setCutWhether(false);
        friendRepository.save(addData);
    }

    public List<FriendItem> getFriends() {
        List<Friend> originList = friendRepository.findAll();
        List<FriendItem> result = new LinkedList<>();

        for (Friend friend : originList) {
            FriendItem addItem = new FriendItem();
            addItem.setName(friend.getName());
            addItem.setEtcMemo(friend.getEtcMemo());
            result.add(addItem);
        }
        return result;
    }

    public FriendResponse getFriend(long id) {
        Friend originData = friendRepository.findById(id).orElseThrow();
        FriendResponse response = new FriendResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setBirthDay(originData.getBirthDay());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setEtcMemo(originData.getEtcMemo());
        response.setCutWhether(originData.getCutWhether());
        response.setCutDate(originData.getCutDate());
        response.setCutReasons(originData.getCutReasons());
        return response;
    }

    public void putBirthDay(long id, FriendBirthDayChangeRequest request) {
        Friend originData = friendRepository.findById(id).orElseThrow();
        originData.setBirthDay(request.getBirthDay());
        friendRepository.save(originData);
    }

    public void putFriendCut(long id, FriendCutChangeRequest request) {
        Friend originDate = friendRepository.findById(id).orElseThrow();
        originDate.setCutWhether(true);
        originDate.setCutDate(LocalDate.now());
        originDate.setCutReasons(request.getCutReasons());
        friendRepository.save(originDate);
    }

    public void putFriendIn(long id) {
        Friend originDate = friendRepository.findById(id).orElseThrow();
        originDate.setCutWhether(false);
        originDate.setCutDate(null);
        originDate.setCutReasons(null);
        friendRepository.save(originDate);
    }
}
