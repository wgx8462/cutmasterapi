package com.gb.cutmasterapi.service;

import com.gb.cutmasterapi.entity.Friend;
import com.gb.cutmasterapi.entity.Humanity;
import com.gb.cutmasterapi.model.humanity.HumanityCreatRequest;
import com.gb.cutmasterapi.model.humanity.HumanityGiveAndTakeChangeRequest;
import com.gb.cutmasterapi.model.humanity.HumanityItem;
import com.gb.cutmasterapi.model.humanity.HumanityResponse;
import com.gb.cutmasterapi.repository.HumanityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HumanityService {
    private final HumanityRepository humanityRepository;

    public void setHumanity(Friend friend, HumanityCreatRequest request) {
        Humanity addData = new Humanity();
        addData.setFriend(friend);
        addData.setGiveAndTake(request.getGiveAndTake());
        addData.setItemName(request.getItemName());
        addData.setAmount(request.getAmount());
        addData.setGntToday(request.getGntToday());
        humanityRepository.save(addData);

    }

    public List<HumanityItem> getHumanitys() {
        List<Humanity> originList = humanityRepository.findAll();
        List<HumanityItem> result = new LinkedList<>();

        for (Humanity humanity : originList) {
            HumanityItem addItem = new HumanityItem();
            addItem.setFriendName(humanity.getFriend().getName());
            addItem.setFriendPhoneNumber(humanity.getFriend().getPhoneNumber());
            addItem.setGiveAndTake(humanity.getGiveAndTake().getName());
            addItem.setItemName(humanity.getItemName());
            result.add(addItem);
        }
        return result;
    }

    public HumanityResponse getHumanity(long id) {
        Humanity originData = humanityRepository.findById(id).orElseThrow();
        HumanityResponse response = new HumanityResponse();

        response.setFriendName(originData.getFriend().getName());
        response.setFriendPhoneNumber(originData.getFriend().getPhoneNumber());
        response.setId(originData.getId());
        response.setGiveAndTake(originData.getGiveAndTake().getName());
        response.setItemName(originData.getItemName());
        response.setAmount(originData.getAmount());
        response.setGntToday(originData.getGntToday());
        return response;
    }

    public void putGiveAndTake(long id, HumanityGiveAndTakeChangeRequest request) {
        Humanity originData = humanityRepository.findById(id).orElseThrow();
        originData.setGiveAndTake(request.getGiveAndTake());
        originData.setItemName(request.getItemName());
        originData.setAmount(request.getAmount());
        humanityRepository.save(originData);
    }

    public void delHumanity(long id) {
        humanityRepository.deleteById(id);
    }
}
