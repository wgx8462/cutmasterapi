package com.gb.cutmasterapi.entity;

import com.gb.cutmasterapi.enums.GiveAndTake;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Humanity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "friendId", nullable = false)
    private Friend friend;
    @Column(nullable = false, length = 3)
    @Enumerated(value = EnumType.STRING)
    private GiveAndTake giveAndTake;
    @Column(nullable = false, length = 30)
    private String itemName;
    @Column(nullable = false)
    private Double amount;
    @Column(nullable = false)
    private LocalDate gntToday;
}
