package com.gb.cutmasterapi.repository;

import com.gb.cutmasterapi.entity.Friend;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FriendRepository extends JpaRepository<Friend, Long> {
}
