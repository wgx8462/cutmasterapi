package com.gb.cutmasterapi.repository;

import com.gb.cutmasterapi.entity.Humanity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HumanityRepository extends JpaRepository<Humanity, Long> {
}
