package com.gb.cutmasterapi.model.humanity;

import com.gb.cutmasterapi.enums.GiveAndTake;
import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class HumanityCreatRequest {
    private Long friendId;
    @Column(nullable = false, length = 3)
    @Enumerated(value = EnumType.STRING)
    private GiveAndTake giveAndTake;
    private String itemName;
    private Double amount;
    private LocalDate gntToday;
}
