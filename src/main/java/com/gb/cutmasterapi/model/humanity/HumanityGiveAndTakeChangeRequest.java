package com.gb.cutmasterapi.model.humanity;

import com.gb.cutmasterapi.enums.GiveAndTake;
import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HumanityGiveAndTakeChangeRequest {
    @Column(nullable = false, length = 3)
    @Enumerated(value = EnumType.STRING)
    private GiveAndTake giveAndTake;
    private String itemName;
    private Double amount;
}
