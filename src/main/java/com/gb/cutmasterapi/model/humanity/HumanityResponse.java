package com.gb.cutmasterapi.model.humanity;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class HumanityResponse {
    private String friendName;
    private String friendPhoneNumber;
    private Long id;
    private String giveAndTake;
    private String itemName;
    private Double amount;
    private LocalDate gntToday;


}
