package com.gb.cutmasterapi.model.humanity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HumanityItem {
    private String friendName;
    private String friendPhoneNumber;
    private String giveAndTake;
    private String itemName;
}
