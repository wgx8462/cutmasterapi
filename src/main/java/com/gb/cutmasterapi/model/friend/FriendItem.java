package com.gb.cutmasterapi.model.friend;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FriendItem {
    private String name;
    private String etcMemo;
}
