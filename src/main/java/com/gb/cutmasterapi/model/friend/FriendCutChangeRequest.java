package com.gb.cutmasterapi.model.friend;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FriendCutChangeRequest {
    private String cutReasons;
}
