package com.gb.cutmasterapi.model.friend;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class FriendResponse {
    private Long id;
    private String name;
    private LocalDate birthDay;
    private String phoneNumber;
    private String etcMemo;
    private Boolean cutWhether;
    private LocalDate cutDate;
    private String cutReasons;
}
