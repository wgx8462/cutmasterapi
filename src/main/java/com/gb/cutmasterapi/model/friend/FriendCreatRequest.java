package com.gb.cutmasterapi.model.friend;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class FriendCreatRequest {
    private String name;
    private LocalDate birthDay;
    private String phoneNumber;
    private String etcMemo;
}
