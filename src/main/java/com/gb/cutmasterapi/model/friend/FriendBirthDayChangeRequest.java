package com.gb.cutmasterapi.model.friend;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class FriendBirthDayChangeRequest {
    public LocalDate birthDay;
}
