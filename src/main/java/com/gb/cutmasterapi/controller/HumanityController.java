package com.gb.cutmasterapi.controller;

import com.gb.cutmasterapi.entity.Friend;
import com.gb.cutmasterapi.model.humanity.HumanityCreatRequest;
import com.gb.cutmasterapi.model.humanity.HumanityGiveAndTakeChangeRequest;
import com.gb.cutmasterapi.model.humanity.HumanityItem;
import com.gb.cutmasterapi.model.humanity.HumanityResponse;
import com.gb.cutmasterapi.service.FriendService;
import com.gb.cutmasterapi.service.HumanityService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/humanity")
public class HumanityController {
    private final FriendService friendService;
    private final HumanityService humanityService;

    @PostMapping("/new/friend-id/{friendId}")
    public String setHumanity(@PathVariable long friendId, @RequestBody HumanityCreatRequest request) {
        Friend friend = friendService.getData(friendId);
        humanityService.setHumanity(friend, request);

        return "OK";
    }

    @GetMapping("/all")
    public List<HumanityItem> getHumanitys() {
        return humanityService.getHumanitys();
    }

    @GetMapping("/detail/humanity-id/{id}")
    public HumanityResponse getHumanity(@PathVariable long id) {
        return humanityService.getHumanity(id);
    }

    @PutMapping("/give-and-take/humanity-id/{id}")
    public String putGiveAndTake(@PathVariable long id, @RequestBody HumanityGiveAndTakeChangeRequest request) {
        humanityService.putGiveAndTake(id, request);
        return "수정 완료";
    }

    @DeleteMapping("/{id}")
    public String delHumanity(@PathVariable long id) {
        humanityService.delHumanity(id);
        return "삭제 완료";
    }
}
