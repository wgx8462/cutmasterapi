package com.gb.cutmasterapi.controller;

import com.gb.cutmasterapi.model.friend.*;
import com.gb.cutmasterapi.service.FriendService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/friend")
public class FriendController {
    private final FriendService friendService;

    @PostMapping("/join")
    public String setFriend(@RequestBody FriendCreatRequest request) {
        friendService.setFriend(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<FriendItem> getFriends() {
        return friendService.getFriends();
    }

    @GetMapping("/detail/{id}")
    public FriendResponse getFriend(@PathVariable long id) {
        return friendService.getFriend(id);
    }

    @PutMapping("/birth-day/{id}")
    public String putBirthDay(@PathVariable long id, @RequestBody FriendBirthDayChangeRequest request) {
        friendService.putBirthDay(id, request);
        return "수정완료";
    }

    @PutMapping("/cut/{id}")
    public String putFriendCut(@PathVariable long id, @RequestBody FriendCutChangeRequest request) {
        friendService.putFriendCut(id, request);
        return "수정완료";
    }
}
